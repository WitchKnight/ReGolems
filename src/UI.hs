{-# LANGUAGE RecordWildCards,LambdaCase,RankNTypes,FlexibleContexts,OverloadedStrings,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module UI where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics
import Debug.Trace

import Control.Lens hiding (transform)
import Control.Monad
import Control.Monad.State.Strict
import Control.Monad.IO.Class
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Animations

import Game

data UILabel fonts = UILabel {
  _centered   :: Bool,
  _lId        :: Int, 
  _lText      :: String,
  _lFont      :: fonts,
  _cSize      :: Maybe Int,
  _lColor     :: Maybe Color
} deriving (Eq,Show)


makeLenses ''UILabel

data UIElem fonts game = UIElem {
  _uiActive   :: Bool,
  _uiLabel    :: Maybe (UILabel fonts),
  _lkGrey     :: Maybe Animation,
  _lkSele     :: Maybe Animation,
  _uiDesc     :: Maybe String,
  uiAction    :: Maybe (Action fonts game),
  uiSelect    :: Game fonts game Bool
} 


type UIElems fonts game = DHS.HashMap UICoord (UIElem fonts game)

instance Show (Action f g) where
  show (GAction _ ) = "Game Action"
  show (MAction _ ) = "Mapped Action"


data Menu fonts game = Menu {
  _mElems       :: UIElems fonts game,
  _cancelAct    :: Maybe (Action fonts game),
  _defaultAct   :: Maybe (Action fonts game)
 } deriving Show

instance {-# OVERLAPPING #-} (FontT fonts, GameState game) => Show (UIElems fonts game) where
  show hmap = concatMap show $ DHS.toList hmap

makeLenses ''UIElem
makeLenses ''Menu


instance FontT a => Show (UIElem a b )where
  show el@UIElem{uiSelect = f} = 
    (show $ el^.uiActive) ++
    (show $ el^.uiDesc) ++
    (show $ uiAction el) ++
    (show $ el^.uiLabel)


getTextIds menu = do
  let l = DHS.toList $ menu^.mElems
  let mlabs = [el^.uiLabel | (_,el) <- l ]
  let   ids = [ l^.lId | (Just l) <- mlabs]
  return ids
    
initMenu cancel def cmElems = do
  forM_ cmElems  $ \(_,el) -> do
    (el^.lkGrey) >?> (discard addAnim) 
    (el^.lkSele) >?> (discard addAnim) 
  let els = DHS.fromList cmElems
  return $ Menu els cancel def







mkBasLabel :: (FontT fonts, GameState game) =>Maybe Color -> Maybe Int -> String -> fonts -> Game fonts game (UILabel fonts)
mkBasLabel  color size txt  gfont= do
  tID <- addText  gfont txt size color
  printG "text added"
  printG tID
  return  $ UILabel {
    _centered = False,
    _lId      = tID,
    _lText    = txt,
    _lFont    = gfont ,
    _cSize    = size,
    _lColor   = color
    }

mkWhiteLabel :: (FontT fonts, GameState game) =>Maybe Int -> String -> fonts -> Game fonts game (UILabel fonts)
mkWhiteLabel = mkBasLabel Nothing


mkStdLabel :: (FontT fonts, GameState game) =>String -> fonts -> Game fonts game (UILabel fonts)
mkStdLabel = mkWhiteLabel (Just 25)

mkBlankLabel :: (FontT fonts, GameState game) =>fonts -> Game fonts game (UILabel fonts)
mkBlankLabel = mkStdLabel ""





failElem :: (FontT fonts , GameState game) => Game fonts game  (UIElem fonts game)
failElem =  return $  UIElem  False
                              Nothing
                              Nothing
                              Nothing
                              Nothing
                              Nothing
                              (return False)




mkAnimEl:: (FontT fonts , GameState game) =>  Animation -> Game fonts game (UIElem fonts game) 
mkAnimEl a = pure $  UIElem False
                            Nothing
                            (Just a)
                            Nothing
                            Nothing
                            Nothing
                            (return False)

mkPlainText:: (FontT fonts , GameState game) => String -> Maybe Int -> Game fonts game  (UIElem fonts game)
mkPlainText txt sz =do
    env <- get
    fts <- liftIO $ HIO.toList (envFonts env)
    let ft  = fst $ fts !! 0
    lb <- mkWhiteLabel sz txt ft
    return $  UIElem  False
                      (Just lb)
                      Nothing
                      Nothing
                      Nothing
                      Nothing
                      (return False)


mkActionText :: (FontT fonts , GameState game) => (Action fonts game) -> String -> Maybe Int ->  Game fonts game (UIElem fonts game)
mkActionText action txt sz = do
  el@UIElem{..} <- mkPlainText txt sz
  return $ UIElem{uiAction = Just action, ..} & uiActive .~ True
                      
                        
  

cleanMenu :: (FontT fonts, GameState game) => Menu font game-> Game fonts game  ()
cleanMenu menu = do
  env <- get
  
  ids <- liftIO $ getTextIds menu
  forM_ ids $ \tID -> do
    t <- getText tID
    liftIO $ HIO.delete (env^.envTexts) tID
    t >?> (liftIO.destroy) 
  printG "cleaned menu"




instance (FontT fonts, GameState game) => Animated (UIElem fonts game) where
  dwtat  transf tm uiEl@(UIElem { _uiLabel = l, _lkSele = lkS, _lkGrey = lkG}) = do
    tg <- gets (^.envWindow)
    let anim = case uiEl^.uiActive of
                  True -> lkS >?> dwtat transf tm 
                  False -> lkG >?> dwtat transf tm
    anim
    
    l >?> \l -> getText (l^.lId) >>= (\t -> t >?>/ (printG "no text found" ) $ \a -> dwtat transf tm a  )
    
                   
getUIPos :: MonadState (GameEnv a b) m => V2 (V2 Float) -> m (V2 Float)            
getUIPos ( V2 (V2 x1 x2) (V2 y1 y2) ) = do
  V2 gWIDTH gHEIGHT <- gets (^.conf.wSize)
  return $ V2 (gWIDTH*(x1/x2)) $ gHEIGHT*y1/y2
    