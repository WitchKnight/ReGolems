{-# LANGUAGE FlexibleContexts,RankNTypes,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Game where

  
import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Monad.State.Strict
import Control.Monad.IO.Class
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.IORef


import Lib
import Animations

type EnvID = Int

class (Eq a,Hashable a, Show a) => FontT a where
  mkFont :: a -> IO Font
  mkFont gfont = do
    ft <- err $ fontFromFile $ (fontPath ++ (show gfont) ++ ".ttf") 
    return ft

imgPath = "assets/images/"
fontPath = "assets/fonts/"
soundPath = "assets/sounds/"

data Config = Config {
  _wSize  :: Coord,
  _tSize  :: Coord,
  _gName  :: String
}

makeLenses ''Config

data GameEnv a b where
  GENV :: (FontT fonts, GameState game) => {
      _envSnds    :: HIO.LinearHashTable Int Music,
      _envTxtr    :: HIO.LinearHashTable Int Texture,
      _envSprts   :: HIO.LinearHashTable Int Sprite,
      _envAnims   :: HIO.LinearHashTable Int Animation,
      _envTexts   :: HIO.LinearHashTable Int Text,
      envFonts    :: HIO.LinearHashTable fonts Font,
      _envWindow  :: RenderWindow,
      _conf       :: Config,
      gState      :: game
    } -> GameEnv fonts game




data Game game fonts gameVal where
  GST :: (FontT fonts, GameState game) =>  { runGST :: GameEnv fonts game -> IO (gameVal,GameEnv fonts game)} -> Game fonts game gameVal


data Action fonts game =  GAction ((FontT fonts, GameState game) => Game fonts game ())
                       |  MAction ((FontT fonts, GameState game) => game -> Game fonts game game)
  
class Show a => GameState a where
  getState  :: FontT fonts => Game fonts a a
  -- getTime :: FontT fonts => Game fonts a Time
  -- setTime :: FontT fonts => Game fonts a ()
 
  wrap      ::  FontT fonts => (a-> IO a) -> Game fonts a ()
  wrap f = do
    env@GENV{gState = s} <- get
    ns <- liftIO $ f s
    modify' (\env -> env {gState = ns})

  superWrap :: FontT fonts => (Game fonts a a) -> Game fonts a ()
  superWrap mf = do
    ns <- mf
    modify' (\env -> env {gState = ns})

  drawG     :: FontT fonts => Game fonts a ()
  initS     :: FontT fonts => Game fonts a a
  initG     :: FontT fonts => Game fonts a ()
  initG = superWrap (initS)

  updateS   :: Time -> a -> IO a 
  updateG   :: FontT fonts => Time -> Game fonts a ()
  updateG dt = wrap (updateS dt)

  cleanS    :: FontT fonts => Game fonts a ()

  mapAction :: FontT fonts => Action fonts a -> Game fonts a a
  mapAction (GAction f) = f >> getState
  mapAction (MAction f) = getState >>= f

  -- handleEvent :: FontT fonts => SFEvent -> Game fonts a ()


mk0Env conf game gfonts = do
  snds <- HIO.new
  txtr <- HIO.new
  sprt <- HIO.new
  txts <- HIO.new
  anim <- HIO.new
  fonts <- HIO.new
  
  let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
  wnd   <- createRenderWindow (VideoMode (truncate (conf^.wSize._x))  (truncate (conf^.wSize._y)) 32) (conf^.gName) [SFDefaultStyle] ctxSettings
  let env =  GENV snds txtr sprt txts anim fonts wnd conf game
  forM_ gfonts $ \gf -> do
    ft <-  mkFont gf
    HIO.insert (envFonts env) gf ft
  return env
    


-- INSTANCES



instance Functor (Game fonts game) where
  fmap f gS@(GST g) = GST $ \env -> do
    (gameVal,env') <- runGST gS env
    return (f gameVal, env')

instance(FontT fonts , GameState game) =>  Applicative (Game fonts game) where
  pure g = GST $ \env -> do
    return (g,env)
  gS <*> gS2 = GST $ \env -> do
    (g2b, env') <- runGST gS env
    (g2, env'') <- runGST gS2 env'
    return (g2b g2, env'')

instance(FontT fonts , GameState game) =>  Monad (Game fonts game) where
  return = pure
  gS >>= f = GST $ \env -> do
    (g,env') <- runGST gS env
    let gS2 = f g
    (g2,env'') <- runGST gS2 env'
    return (g2,env'')

instance(FontT fonts , GameState game) =>  MonadState (GameEnv fonts game) (Game fonts game) where
  get = GST $ \env -> return (env,env)
  put x = GST $ \env -> return ((),x)


instance (FontT fonts , GameState game) =>  MonadIO  (Game fonts game) where
  liftIO (action) = GST $ \env -> do
    a<- action
    return (a,env)

instance (FontT fonts , GameState game) =>  MaybeToM  (Game fonts game) where

makeLenses ''GameEnv

-- convenience
printG :: (FontT fonts , GameState game) => Show a => a -> Game fonts game () 
printG = liftIO.print

printG' a = a >>= printG

-- components management



addComponent stuff container =  do
  env  <- get
  let action  = do
      stuffID <- getMaxId (container env)
      HIO.insert (container env) (stuffID+1) stuff
      return (stuffID + 1)
  
  liftIO action


getComponent cID container = do
  env <- get
  component <- liftIO $ HIO.lookup (container env) cID
  return component
  

addText ::(FontT fonts , GameState game) =>  fonts -> String -> Maybe Int -> Maybe Color -> Game fonts game EnvID
addText gfont txt size color =  do
  env  <- get
  let mkText  = do
      fnt <- HIO.lookup (envFonts env) gfont
      tx <- err $ createText
      fnt >?> setTextFont tx 
      setTextString tx txt
      size >?> setTextCharacterSize tx
      color >?> setTextColor tx
      return tx
  tx <- liftIO mkText
  addComponent tx (^.envTexts)

addTexture :: (MonadState (GameEnv a b) m, MonadIO m) => Texture -> m Int
addTexture txtr = addComponent txtr (^.envTxtr)

addSprite :: (MonadState (GameEnv a b) m, MonadIO m) => Sprite -> m Int
addSprite spr = addComponent spr (^.envSprts)


addSound :: (FontT fonts , GameState game) => String -> Game fonts game EnvID
addSound name = do
  snd <- liftIO $ err $ musicFromFile $ soundPath ++ name
  addComponent snd (^.envSnds)

addAnim ::  (FontT fonts , GameState game) => Animation -> Game fonts game EnvID
addAnim anim = addComponent anim (^.envAnims)

getTxtr ::  (FontT fonts , GameState game) => EnvID -> Game fonts game (Maybe Texture)
getTxtr tID = getComponent tID (^.envTxtr)

getText tID = getComponent tID (^.envTexts)
getSprite tID = getComponent tID (^.envSprts)
getAnim tID = getComponent tID (^.envAnims)
getSnd tID = getComponent tID (^.envSnds)

-- rmComponent :: (FontT fonts , GameState game) => EnvID -> (GameEnv fonts game -> HIO.LinearHashTable EnvID v) -> Game fonts game ()
-- rmComponent cID container =  do
--   env <- get
--   c <- getComponent cID (container env)
--   (return c) >?> (liftIO.destroy)
--   liftIO $ HIO.delete  (container env) cID
  



clean :: (FontT fonts , GameState game) => Game fonts game ()
clean = do
  env <- get
  cleanS
  liftIO $ do
    HIO.mapM_ (\(_,x) -> destroy x) $ env^.envSnds
    print "cleaned sounds"
    HIO.mapM_ (\(_,x) -> destroy x) $ env^.envTxtr
    print "cleaned txtrs"
    HIO.mapM_ (\(_,x) -> destroy x) $ env^.envSprts
    print "cleaned sprts"
    HIO.mapM_ (\(_,x) -> cleanAnim x) $ env^.envAnims
    print "cleaned anims"
    HIO.mapM_ (\(_,x) -> destroy x) $ env^.envTexts
    print "cleaned fonts"
    HIO.mapM_ (\(_,x) -> destroy x) $ envFonts env
    print "cleaned texts"
    destroy (env^.envWindow)
    print "destroyed Window"

-- initialization

runGame :: (FontT fonts , GameState game) => GameEnv fonts game -> Game fonts game () -> IO ()
runGame startEnv startGame= do
  (_,initEnv') <- runGST initG startEnv
  (_,finalEnv) <- runGST startGame initEnv'
  (_,finished) <- runGST clean finalEnv
  return () 

-- Animation


class Animated a where
  dwtat :: (FontT fonts , GameState game) => Transform -> Time -> a -> Game fonts game ()

instance Animated Animation where
  dwtat transf t anim = do
    tg <- gets (^.envWindow)
    liftIO $ do
      spr <- getCurrent t anim
      V2 x y <- getMov t anim
      let V2 w h = anim^.animFlip
      let V2 ox oy = anim^.animOffset 
      let V2 sx sy = anim^.animSize
      let V2 scx scy = anim^.animScale
      let transl = translation (ox + negOrPos w x) (oy + negOrPos h y) 
      centerSprite spr

      drawSprite tg spr $ Just $ renderStates  { transform = transf * transl * (scaling (if w then -1 else 1) (if h then -1 else 1) ) }


instance Animated Text where
  dwtat transf t  txt = do
    tg <- gets (^.envWindow)
    liftIO $ draw tg txt $ Just $  renderStates {transform=transf}