{-# LANGUAGE FlexibleInstances,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module Lib where

import GHC.Generics

import System.Random

import Linear.V2
import qualified Data.HashTable.IO as HIO

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import Control.Monad
import Control.Lens hiding (transform)



someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- HASHTABLES

getMaxId :: HIO.LinearHashTable Int a -> IO Int
getMaxId hashtable = do
  ll <- HIO.toList hashtable
  let ks = [k | (k,v) <- ll]
  return $ case ks of [] -> 0
                      _   -> maximum ks








-- COORDINATES

type Coord = V2 Float
type UICoord = V2 Coord

flipx (V2 x y) = V2 (not x) y
flipy (V2 x y) = V2 x (not y)



data Orientation = AngO Float | OctO [Int] deriving (Show,Read,Eq,Ord,Generic)


-- MAYBE & MONADS

discard f  = \x -> f x >> return ()


class Monad m => MaybeToM m where
  maybeToM ::  (t -> m ()) -> Maybe t -> m ()
  maybeToM f x= case x of
    Just y -> do
      f y
    Nothing -> return ()

  maybeToM' :: a -> (t -> m a) -> Maybe t -> m a
  maybeToM' c f x= case x of
    Just y -> do
      f y
    Nothing -> return c

  maybeToMor :: (t  -> m b) -> m b -> Maybe t -> m b
  maybeToMor f or x= case x of
    Just y -> do
      f y
    Nothing -> or
      
  (>?>) :: Maybe a -> (a -> m ()) -> m ()
  (>?>) x f = return x >>= maybeToM f

  (>?>=) :: Maybe a -> b -> (a -> m b) -> m b
  (>?>=) x or f = return x >>= maybeToM' or f

  (>?>/) :: Maybe t -> m b -> (t -> m b) -> m b
  (>?>/) x or f = return x >>= maybeToMor f or

class (MonadPlus m, MaybeToM m) =>  MaybeToMPlus m where
  (>?>==) ::  Maybe a -> (a -> m b) -> m b
  a >?>== f = case a of
    Nothing -> mzero >>= f
    Just x  -> return x >>= f

instance MaybeToM IO where
instance MaybeToM Maybe where
instance MaybeToM [] where
instance MaybeToMPlus [] where
instance MaybeToMPlus Maybe where

-- 

centerSprite spr = do
  tr <- getTextureRect spr
  let w = iwidth tr
  let h = iheight tr
  let vec = Vec2f ((fromIntegral w)/2) ((fromIntegral h)/2) 
  setOrigin spr vec

negOrPos True x = -x
negOrPos False x = x