{-# LANGUAGE FlexibleInstances,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Animations where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS

import Data.HashMap.Strict((!))
import Data.Hashable

import Data.IORef

import Lib

tSZ :: Int
tSZ = 23
tSZ' :: Float
tSZ' = 23

data Animation = Animation {
  _animScale  :: V2 Float,
  _animSize   :: V2 Int,
  _animOffset :: V2 Float,
  _animText   :: Texture,
  _animDurs   :: [Int],
  _animMov    :: [V2 Float],
  _animFlip   :: V2 Bool,
  _animSprite :: Sprite
}

instance Show Animation where
  show (Animation scale size offset _ durs mov flip _) = 
    (show size) ++ (show offset) ++ (show durs) ++ (show mov) ++ (show flip)

makeLenses ''Animation

initSizedAnim texture info mov size offset= do
  anim <- initAnim texture info mov
  return $ anim & animSize .~ size 
                & animOffset .~ offset

initAnim' texture info  = initAnim texture info []
initAnim texture info mov= do
  emv <- newEmptyMVar
  spr <- err $ createSprite
  anim <- return $ Animation {
      _animScale = V2 0 0,
      _animSize = V2 tSZ tSZ,
      _animOffset = V2 0 0,
      _animText = texture,
      _animDurs = info,
      _animMov = mov,
      _animFlip = V2 False False,
      _animSprite = spr
    }
  setTexture (anim^.animSprite) (anim^.animText) True
  return anim


flipved anim = anim & animFlip %~ flipy
fliphed anim = anim & animFlip %~ flipx

getCurrent time anim = case anim^.animDurs of
    [] -> return $ anim^.animSprite
    durations -> do
      let framesPassed =(60*asMilliseconds time) `div` (1000)
      -- we tie in the loop to the total number of frames of the animation
      let currentFrame = framesPassed `rem` (sum durations)
      let index = findIndex currentFrame durations
      Vec2u imgw' imgh <- textureSize (anim^.animText)
      let V2 sx sy = anim^.animSize
      let imgw =fromIntegral $  div imgw' $ fromIntegral  sx
      let row = index `div` imgw
      let col = index `rem` imgw
      setTextureRect (anim^.animSprite)  $ IntRect (sx*col) (sy*row) sx sy
      return $ anim^.animSprite


getIndex durations time = index 
      where
        framesPassed =(60*asMilliseconds time) `div` (1000)
      -- we tie in the loop to the total number of frames of the animation
        currentFrame = framesPassed `rem` (sum durations)
        index = findIndex currentFrame durations


getMov time anim  = case anim^.animDurs of
  [] -> return (V2 0 0)
  durations -> case anim^.animMov of
    [] -> return (V2 0 0)
    moves -> do
      let index = getIndex durations time
      return $ moves !! index

findIndex frames durations = findIndex' frames 0 durations 0
findIndex' frames i [] accum = 0      
findIndex' frames i (d:ds) accum
  | accum + d >= frames = i
  | otherwise = findIndex' frames (i+1) ds (d+accum)

cleanAnim anim = do
  destroy (anim^.animSprite)
  destroy (anim^.animText)

