{-# LANGUAGE NondecreasingIndentation,LambdaCase,ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module App where

import Debug.Trace

import GHC.Generics
import Unsafe.Coerce
import Control.Lens
import Control.Monad
import Control.Monad.State.Strict
import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time
  
import Animations

import Game
import Lib
import UI

import Linear.V2
import qualified Data.HashMap.Strict as DHS
import qualified Data.HashTable.IO as HIO
import Data.Hashable


data Fonts = MenuFont | DragonFont deriving (Eq,Show,Read,Ord,Generic)

instance Hashable Fonts

instance FontT Fonts

data TestG = TG Time MenuG deriving (Show)

instance GameState TestG where
  getState = do
    env <- get
    return $ gState env
 
  updateS dt (TG t m)= do
    
    
    return $ TG (t+dt) m
  drawG = do
    wnd <- gets (^.envWindow)
    TG t m <- getState
    liftIO $ clearRenderWindow wnd black
    let f crd el= do
        V2 x y <- getUIPos crd 
        let transf =  (translation x y)
        dwtat transf t el
    DHS.traverseWithKey f $ m^.mElems
    liftIO $ display wnd

  initS = do
    TG t _<- getState
    menu <- unsafeCoerce tM1
    return $ TG t menu 

  cleanS = do
    TG _ m <- getState
    cleanMenu m




type MenuG = Menu Fonts TestG
type GameM = Game Fonts TestG
type GUI   = UIElem Fonts TestG

getTime :: GameM Time
getTime = do
  (TG t _) <- getState
  return t

setTime :: Time -> GameM ()
setTime t = wrap $ (\(TG _ m) -> return $ TG t m )

modTime :: (Time -> Time) -> GameM ()
modTime f = do
  t <- getTime
  setTime (f t)

testAction = GAction $ printG "yes"

initOgre = do
  bntex <- liftIO $ err $ textureFromFile (imgPath ++ "ogre.png") Nothing
  ogranim <- liftIO $ initAnim' bntex (replicate 4 20)
  return ogranim

whenT f = do
  TG t m <- getState
  printG t
  return $ f t



tmBlock :: GameM [((Float, Float, Float, Float), GameM GUI,GameM Bool)]
tmBlock = do
  anim1 <- initOgre
  return $ [
    ((1,2,2,7),mkPlainText "YES"  $ Just 45, return False),
    ((1,2,3,7),mkPlainText "NO"   $ Just 25, return False),
    ((1,3,3,7),mkAnimEl anim1, return False),
    ((1,4,6,7),mkActionText  (testAction )"CLICK Ma" $ Just 15, whenT (\t -> t `rem` 2 == 0)),
    ((1,4,3,7),mkActionText  (testAction )"CLICK MO" $ Just 25, whenT (\t -> True))
    ]


tM1 :: GameM MenuG
tM1  = do
  block <- tmBlock
  basicMenu block

zeroMenu :: MenuG
zeroMenu = Menu (DHS.fromList []) Nothing Nothing 



basicMenu :: [((Float, Float, Float, Float),
                GameM GUI, GameM Bool)] -> GameM MenuG
basicMenu cels =  do
  let convert ((x,y,x',y'), act,selec) = do
      elem <- act
      let elem' = elem{uiSelect = selec}
      return $ (V2 (V2 x y) (V2 x' y'), elem')
  cels' <- mapM convert cels
  let elems = DHS.fromList cels'
  return $ Menu elems Nothing Nothing 



confirmMenu :: GameM ()
confirmMenu = superWrap $ do
  g@(TG t m) <- getState
  let elems = DHS.toList $ m^.mElems
  let activeElems = (filter (^.uiActive) $ map snd elems)
  printG activeElems
  let filtSel = \el@UIElem{uiSelect = f} -> f >>= pure.traceShowId
  selecElems <- filterM filtSel activeElems
  printG selecElems
  nG <- case selecElems of
    [] ->(m^.defaultAct) >?>= g $ \a -> printG "fm" >> mapAction a
    _  -> return g
  let knot g el@UIElem{uiAction = a} = a >?>= g $  \a -> printG "sm" >> mapAction a               
  cG <- foldM knot nG selecElems 
  return cG