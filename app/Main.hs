{-# LANGUAGE NondecreasingIndentation,LambdaCase,ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}


module Main where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import System.Random
import System.Exit

import System.IO.Unsafe

import Debug.Trace
import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Monad.IO.Class

import Control.Monad.State.Strict

import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Control.Concurrent

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.List(find,elemIndex)
import Data.IORef
import Control.Concurrent.KazuraQueue
  
import App

import Game
import Lib
import Init

conf0 = Config (V2 800 600) (V2 23 23) "Test"


main :: IO ()
main = do
  clock <- createClock
  let fonts = [MenuFont]
  let menu = zeroMenu
  env <- mk0Env conf0 (TG 0 menu) fonts
  runGame env $ loop clock 0 


loop :: Clock -> Time -> GameM ()
loop clock dt = do
  dt <- liftIO $ restartClock clock
  w <- gets (^.envWindow)
  time <- getTime
  updateG dt 
  drawG
  return (asSeconds time > 10) >>= \case 
      True -> return ()
      False -> do
        evt   <- liftIO $ pollEvent w
        case evt of
          Just SFEvtClosed  -> return ()
          Just( SFEvtMouseButtonPressed _  _ _) -> confirmMenu >> loop clock dt 
          _                 -> loop clock dt 