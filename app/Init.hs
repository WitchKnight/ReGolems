{-# LANGUAGE NondecreasingIndentation,LambdaCase,ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module Init where

import GHC.Generics

import Control.Lens
import Control.Monad
import Control.Monad.State.Strict
import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time
import qualified Data.HashTable.IO as HIO
import Game
import Lib
import UI
import App

import Linear.V2
import Data.Hashable




